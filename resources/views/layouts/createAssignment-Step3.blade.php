@extends('layouts.templateProfesor')


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>




@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 3</h2>

                <form class="form-horizontal" action="{{ url('createAssignment-Step3') }}" method="post"  enctype="multipart/form-data">
                    @csrf
                    <div>
                        <div class="form-group" style="margin-top: 20px">
                            <label for="delivered_date">{{ __('Fecha de entrega') }}</label>
                            <input id="delivered_date" type="datetime-local" min="<?php echo date("Y-m-d\TH:i");?>"  class="form-control{{ $errors->has('delivered_date') ? ' is-invalid' : '' }}" name="delivered_date" value="{{ old('delivered_date') }}" requisi>
                        @if ($errors->has('delivered_date'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('delivered_date') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div style="margin-top: 20px">
                        <label for="file">Archivo de corrección</label>
                        <input id="file" type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" value="{{ old('file') }}" requisi>
                        @if ($errors->has('file'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <a   href="createAssignment-Step1" role="button" class="btn btn-primary">Volver al paso 1</a>
                            <a   href="createAssignment-Step2" role="button" class="btn btn-primary">Volver al paso 2</a>
                            <button type="submit"  style="color: black" class="btn btn-primary">
                                {{ __('Crear práctica') }}
                            </button>
                        </div>
                    </div>
                    <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
