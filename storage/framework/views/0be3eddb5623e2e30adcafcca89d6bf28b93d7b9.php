<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>


<script type="text/javascript">
    function BuildFormFields($amount)
    {

        var
            $container = document.getElementById('FormFields'),
            $item, $field, $i;
         $container.class = "form-group";



        $container.innerHTML = '';
        for ($i = 1; $i < $amount+1; $i++) {
            $item = document.createElement('div');
            $item.class = 'form-group';
            $item.style = 'margin-top:20px';

            $field = document.createElement('label');
            $field.innerHTML = 'Nombre de archivo '+ $i +' a entregar y extensión:';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'nameArchivo' + $i + '';
            $field.type = 'text';
            $field.placeholder ='Ej) practica.c';
            //$field.value='<?php echo e(old('nameArchivo')); ?>';
            $item.appendChild($field);

            $field = document.createElement('label');
            $field.innerHTML = 'Ponderación del archivo '+ $i +':';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'ponderacionArchivo' + $i + '';
            $field.type = 'number';
            $field.min = '1'
            $field.max = '100'
            $field.placeholder ='100%';
            //$field.value='<?php echo e(old('ponderacionArchivo')); ?>';
            $item.appendChild($field);

            $container.appendChild($item);
        }
    }

</script>

<?php $__env->startSection('content'); ?>
    <?php
    if($practica['name']){
        $name = $practica['name'];
    }else{
        $name = old('name');
    }
    if($practica['number']){
        $number = $practica['number'];
    }else{
        $number = old('number');
    }
    if($practica['attempts']){
        $attempts = $practica['attempts'];
    }else{
        $attempts = old('attempts');
    }
    ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 1</h2>

                <form class="form-horizontal" action="<?php echo e(url('crearPractica-paso1')); ?>" method="post"  enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div  style="margin-top: 20px">
                        <div class="form-group">
                        <label for="name"><?php echo e(__('Nombre de la práctica')); ?></label>

                        <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e($name); ?>" requisi autofocus>
                        <?php if($errors->has('name')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>
                    <div>
                        <div class="form-group">
                            <label for="number"><?php echo e(__('Número de archivos a entregar')); ?></label>
                            <input onkeyup="BuildFormFields(parseInt(this.value, 10));" id="number" type="number" class="form-control<?php echo e($errors->has('number') ? ' is-invalid' : ''); ?>" min="1" placeholder=">=1" name="number" value="<?php echo e($number); ?>" requisi autofocus/>

                            <?php if($errors->has('number')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('number')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if( Session::has('error') ): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong><?php echo e(Session::get('error')); ?></strong>
                        </div>
                    <?php endif; ?>


                    <div id="FormFields">
                <?php if($errors->has('nameArchivo')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('nameArchivo')); ?></strong>
                                    </span>
                            <?php endif; ?>
                    </div>
                    <div>
                        <div class="form-group" style="margin-top: 20px">
                        <label for="attempts"><?php echo e(__('Intentos')); ?></label>
                            <input id="attempts" type="number" class="form-control<?php echo e($errors->has('attempts') ? ' is-invalid' : ''); ?>" min='1' placeholder=">=1" name="attempts" value="<?php echo e($attempts); ?>" requisi>
                            <?php if($errors->has('attempts')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('attempts')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>


                            <div class="form-group" style="margin-top: 20px">
                                <label for="language"><?php echo e(__('Lenguaje de programación: ')); ?></label>
                                <div>
                                <select name="language" id="language">
                                        <option value="c" selected>C</option>
                                        <option value="c#">C#</option>
                                        <option value="java">Java</option>

                                </select>
                                <?php if($errors->has('language')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('language')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            </div>

                    <div class="form-group" style="margin-top: 20px">
                        <label for="period"><?php echo e(__('Convocatoria: ')); ?></label>
                        <div>
                        <select name="period" id="period">
                            <option value="ordinaria" selected>Ordinaria</option>
                            <option value="extraordinaria">Extraordinaria</option>
                        </select>
                        <?php if($errors->has('period')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('period')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>
                    <div class="form-group" style="margin-top: 20px">
                        <label for="subject"><?php echo e(__('Asignatura: ')); ?></label>
                        <div>
                        <select name="subject" id="subject">
                            <?php $__currentLoopData = $asignaturas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asignatura): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($asignatura); ?>"><?php echo e($asignatura); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('subject')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('subject')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>



                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <button type="submit"  style="margin-left: 70px; width: 200px;" class="btn btn-primary">
                                <?php echo e(__('Siguiente paso')); ?>

                            </button>
                    </div>
                    </div>
                        <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateProfesor', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>