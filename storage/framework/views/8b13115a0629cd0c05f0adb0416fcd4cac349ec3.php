<?php $__env->startSection('title','Formulario'); ?>

<?php $__env->startSection('content'); ?>


    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if(count($asignaturas)!= 0): ?>
    <form class="form-group" method="POST" action="<?php echo e(dirname($_SERVER['PHP_SELF']) . '/practicas'); ?>" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="card" style="margin-left: auto; margin-right: auto; width: 60rem; margin-top: 70px; padding: 4%;">


                    <div class="row">
                        <div class="col-sm">
                            <h5 class="card-title">Resultado de prácticas por asignatura</h5>

                            <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                                <select name="asignatura" size="<?php echo count($asignaturas)?>">
                                    <?php $__currentLoopData = $asignaturas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asignatura): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($asignatura->id); ?>"><?php echo e($asignatura->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <input name="vista" value="resultado" hidden/>
                                <button type="submit" style="margin-top: 2%" class="btn btn-primary">Seleccionar</button>
                            </div>
                        </div>
                    </div>

        </div>
    </form>
           <?php else: ?>
        <div class="row">
            <div class="col-sm">
                <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                <h5 class="card-title">No existen asignaturas</h5>
                </div>
            </div>
        </div>
            <?php endif; ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>