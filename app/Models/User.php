<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Apr 2019 00:13:36 +0200.
 */

namespace pfg\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $surname
 * @property string $dni
 * @property int $roles_id
 * 
 * @property \pfg\Models\Role $role
 * @property \Illuminate\Database\Eloquent\Collection $rel_users_groups
 * @property \Illuminate\Database\Eloquent\Collection $subjects
 * @property \Illuminate\Database\Eloquent\Collection $alumnos
 * @property \Illuminate\Database\Eloquent\Collection $profesors
 *
 * @package pfg\Models
 */
class User extends Authenticatable
{
	protected $casts = [
		'roles_id' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'remember_token',
		'surname',
		'dni',
		'roles_id',
        'token'
	];

	public function role()
	{
		return $this->belongsTo(\pfg\Models\Role::class, 'roles_id');
	}

	public function rel_users_groups()
	{
		return $this->hasMany(\pfg\Models\RelUsersGroup::class, 'users_id');
	}

	public function subjects()
	{
		return $this->belongsToMany(\pfg\Models\Subject::class, 'rel_users_subject', 'users_id')
					->withPivot('id')
					->withTimestamps();
	}

	public function alumnos()
	{
		return $this->hasMany(\pfg\Models\Alumno::class, 'users_id');
	}

	public function profesors()
	{
		return $this->hasMany(\pfg\Models\Profesor::class, 'users_id');
	}
}
