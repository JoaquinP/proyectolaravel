<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArchivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('archivos', function(Blueprint $table)
		{
			$table->foreign('practicas_id', 'fk_archivos_practicas1')->references('id')->on('practicas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('archivos', function(Blueprint $table)
		{
			$table->dropForeign('fk_archivos_practicas1');
		});
	}

}
