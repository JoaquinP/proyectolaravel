<?php $__env->startSection('title','Formulario'); ?>

<?php $__env->startSection('content'); ?>


    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if(count($archivos)!= 0): ?>
      <?php $__currentLoopData = $archivos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $archivo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <form class="form-group" method="POST" action="<?php echo e(dirname($_SERVER['PHP_SELF']) . '/guardar'); ?>" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
              <div class="card" style="margin-left: auto; margin-right: auto; width: 60rem; margin-top: 70px; padding: 4%;">

              <div class="row">
                        <div class="col-sm">
                            <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                                <img style="height:30px; width: 30px; margin: 20px;" class="card-img-top rounded-circle mx-auto d-block" src="test/<?php echo e($archivo->extension); ?>">
                                <h5 class="card-title">Nombre: <?php echo e($archivo->name); ?></h5>
                                <input type="hidden" name="name" value="<?php echo e($archivo->name); ?>">
                                <h5 class="card-title">Fecha límite: <?php echo e($archivo->expired_date); ?></h5>
                                <input type="hidden" name="expired_date" value="<?php echo e($archivo->expired_date); ?>">
                                <h5 class="card-title">Peso: <?php echo e($archivo->weight); ?></h5>
                                <input type="hidden" name="weight" value="<?php echo e($archivo->weight); ?>">
                                <h5 class="card-title">Intentos restantes: <?php echo e($archivo->intentos); ?></h5>
                                <input type="hidden" name="intentos" value="<?php echo e($archivo->intentos); ?>">
                                <input type="hidden" name="extension" value="<?php echo e($archivo->extension); ?>">
                                <input type="hidden" name="id" value="<?php echo e($archivo->id); ?>">



                                <?php   $idRol = DB::table('profesor')->where('users_id',auth()->id())->value('roles_id');
                                if($idRol == null){
                                    $idRol = DB::table('alumno')->where('users_id',auth()->id())->value('roles_id');
                                };?>

                                <?php if($idRol == 2): ?>
                                    <div class="form-group">
                                        <h5 class="card-title">Código fuente: <?php echo e($archivo->file_prof); ?></h5>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 col-form-label">Código fuente:</label>
                                        <input type="file"  name="file" >
                                    </div>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
              </div>
          </form>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

           <?php else: ?>
        <div class="row">
            <div class="col-sm">
                <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                <h5 class="card-title">No existen prácticas</h5>
                </div>
            </div>
        </div>
            <?php endif; ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>