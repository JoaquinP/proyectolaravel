<?php $__env->startSection('title','Formulario'); ?>

<?php $__env->startSection('content'); ?>


    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>




        <div class="card" style="margin-left: auto; margin-right: auto; width: 60rem; margin-top: 70px; padding: 4%;">
                    <div class="row">
                        <div class="col-sm">

                        <?php if($notafinal>=0): ?>
                                <h5 class="card-title">Resultados de la práctica <?php echo e($pname); ?></h5>
                                <h5 class="card-title">Número de archivos:<?php echo e(count($archivos)); ?></h5>
                            <h5 class="card-title">Nota final: <?php echo e(number_format($notafinal,2)); ?></h5>
                            <?php else: ?>
                                <h5 class="card-title">Resultados</h5>
                            <?php endif; ?>
                            <?php $__currentLoopData = $archivos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $archivo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                                <h3 class="card-title"><?php echo e($archivo->name); ?></h3>
                                <h5 class="card-title">TOTAL: <?php echo e($archivo->total); ?></h5>
                                <h5 class="card-title">Pasados: <?php echo e($archivo->pasados); ?></h5>
                                <h5 class="card-title">Fallados: <?php echo e($archivo->fallados); ?></h5>
                                <?php if($archivo->fallados!=0): ?>
                                    <?php $__currentLoopData = $failresults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <h5 class="card-title">Descripción: <?php echo e($fail); ?></h5>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <h5 class="card-title">Intentos restantes: <?php echo e($archivo->intentos); ?></h5>
                                <h5 class="card-title">Nota: <?php echo e(number_format($archivo->nota,2)); ?></h5>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
        </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>