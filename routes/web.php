<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/primera',function (){
    return "Hola";
});


Route::get('/nombre/{nombre}',function ($nombre){
    return 'Hola soy '. $nombre;
});

//Route::get('formulario', 'AdminController@index');

//Route::post('create', 'AdminController@store');z


Route::resource('admin', 'AdminController');

Route::resource('alumno', 'AlumnoController');
Auth::routes();

Route::get('/home', 'AdminController@index')->name('home');

Route::post('/practicas','AlumnoController@practicas');
Route::post('/archivos','AlumnoController@archivos');
Route::post('/resultados','AlumnoController@resultados');

Route::post('/guardar','AlumnoController@store');

Route::get('/mostrar','AlumnoController@show');

Route::get('logout', '\pfg\Http\Controllers\Auth\LoginController@logout');


Route::get('/createAssignment-Step1', 'InstructorController@createAssignmentStep1');
Route::post('/createAssignment-Step1', 'InstructorController@postcreateAssignmentStep1');

Route::get('/createAssignment-Step2', 'InstructorController@createAssignmentStep2');
Route::post('/createAssignment-Step2', 'InstructorController@postcreateAssignmentStep2');

Route::get('/createAssignment-Step3', 'InstructorController@createAssignmentStep3');
Route::post('/createAssignment-Step3', 'InstructorController@postcreateAssignmentStep3');

Route::get('/registerInstructor','InstructorController@register');
Route::get('/usuariosInstructor','InstructorController@users');

Route::get('/showSubjects','InstructorController@showSubjects');
Route::get('/showSubjectsStudent','StudentController@showSubjects');

Route::get('selectedSubjectStudent/{subject}', 'StudentController@selectedSubject');
Route::post('selectedSubjectStudent/{subject}', 'StudentController@selectedSubject');

//Route::get('/selectedSubject', 'InstructorController@selectedSubject');
//Route::post('/selectedSubject', 'InstructorController@selectedSubject');

Route::get('selectedSubject/{request}', 'InstructorController@selectedSubject');

Route::get('/showAssignmentsStudent', 'StudentController@showAssignmentsStudent');
Route::post('/showAssignmentsStudent', 'StudentController@showAssignmentsStudent');
Route::get('/showAssignments', 'InstructorController@showAssignments');
Route::post('/showAssignments', 'InstructorController@showAssignments');

Route::get('/showStudentsFiles', 'StudentController@showStudentsFiles');
Route::post('/showStudentsFiles', 'StudentController@showStudentsFiles');

Route::get('/sendStudentFiles', 'StudentController@sendStudentFiles');
Route::post('/sendStudentFiles', 'StudentController@sendStudentFiles');

Route::get('/showResultsStudent', 'StudentController@showResultsStudent');
Route::post('/showResultsStudent', 'StudentController@showResultsStudent');

Route::get('/showResultsStudentSubjects','StudentController@showResultsStudentSubjects');


Route::get('/showResultsAssignmentsStudent', 'StudentController@showResultsAssignmentsStudent');
Route::post('/showResultsAssignmentsStudent', 'StudentController@showResultsAssignmentsStudent');

Route::get('/createUsers', 'AdminController@createUsers');
Route::post('/createUsers', 'AdminController@createUsers');

Route::get('/users', 'AdminController@index');
Route::post('/users', 'AdminController@index');

Route::get('createUsers/restablishpass/{token}',['as' => 'restablishpass','uses' => 'AdminController@restablishPass']);
Route::post('createUsers/restablishpass/{token}',['as' => 'restablishpass','uses' => 'AdminController@restablishPass']);

Route::get('createUsers/restablishpass/remember',['as' => 'remember','uses' => 'AdminController@remember']);
Route::post('createUsers/restablishpass/remember',['as' => 'remember','uses' => 'AdminController@remember']);

Route::get('/perdida', 'AdminController@perdida');
Route::post('/perdida', 'AdminController@perdida');



Route::post('sendPerdidaEmail',['as' => 'sendPerdidaEmail', 'uses' => 'AdminController@sendPerdidaEmail']);
Route::get('sendPerdidaEmail',['as' => 'sendPerdidaEmail', 'uses' => 'AdminController@sendPerdidaEmail']);