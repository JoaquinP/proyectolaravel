<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("select").change(function(){
            $(this).find("option:selected").each(function(){
                var optionValue = $(this).attr("value");
                if(optionValue){
                    $(".box").not("." + optionValue).hide();
                    $("." + optionValue).show();
                } else{
                    $(".box").hide();
                }
            });
        }).change();
    });
</script>

<script type="text/javascript">

    function BuildFormFields2($amount)
    {
        var alumno = document.getElementById("alumnos").value;
        var groups = Math.ceil(alumno/$amount);

        var
            $container = document.getElementById('FormFields2'),
            $item, $field,$option,$campo, $i, $j,$k;
        $container.class = "form-group";

        $container.innerHTML = '';
        var k = 0;
    <?php $f=1?>
        for ($j = 1; $j < groups+1; $j++) {

            $item = document.createElement('div');
            $item.class = 'form-group';
            $item.style = 'margin-top:20px';

            $campo = document.createElement('h2');
            $campo.innerHTML = 'Grupo ' + $j + ':';
            $item.appendChild($campo);

            for ($i = 1; $i < $amount + 1;  $i++) {
                k++;
                if(k <= alumno){

                    $field = document.createElement('label');
                    $field.innerHTML = 'Introduce el nombre del componente ' + $i + ' del grupo ' + $j ;
                    $item.appendChild($field);
                    $field = document.createElement('select');
                    $field.className = 'form-control';
                    $field.name = 'users_id.' + $j + '.'+ $i ;
                    $field.type = 'text';
                    $field.value='<?php echo e($relUsersGroup[1][1]['users_id']); ?>';
                    $item.appendChild($field);
                    <?php foreach($users as $user){ ?>
                    $option = document.createElement('option');
                    $option.value = '<?php echo e($user->users_id); ?>';
                    $option.text = '<?php echo e($user->name); ?>';
                    $field.appendChild($option);
                    <?php } ?>

                    <?php $f++?>
                    $container.appendChild($item);
                }
            }
        }
    }

</script>

<?php $__env->startSection('content'); ?>

    <?php
    //dd($assignment);
    if($assignment['type']){
        $type = $assignment['type'];

    }else{
        $type = old('type');
    }
    if($groupAssignment['members_number']){
        $members_number = $groupAssignment['members_number'];
    }else{
        $members_number = old('members_number');
    }
    ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 2</h2>

                <form class="form-horizontal" action="<?php echo e(url('createAssignment-Step2')); ?>" method="post"  enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="form-group" style="margin-top: 20px">
                        <label for="type"><?php echo e(__('Tipo de práctica: ')); ?></label>
                        <div>
                            <select name="type" id="type">
                                <?php if($type == 'grupo'): ?>
                                    <option value="individual">Individual</option>
                                    <option value="grupo" selected>Grupo</option>
                                <?php else: ?>
                                    <option value="individual" selected>Individual</option>
                                    <option value="grupo" >Grupo</option>
                                <?php endif; ?>
                            </select>
                            <?php if($errors->has('type')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('type')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="grupo box">
                        <div class="form-group" style="margin-top: 20px">
                            <label for="members_number"><?php echo e(__('¿Cuántas personas van a formar el grupo?')); ?></label>
                            <input onclick="BuildFormFields2(parseInt(this.value, 10));" onkeyup="BuildFormFields2(parseInt(this.value, 10));" id="members_number" type="number" class="form-control<?php echo e($errors->has('members_number') ? ' is-invalid' : ''); ?>" min='1' placeholder=">=1" name="members_number" value="<?php echo e($members_number); ?>" requisi>
                            <?php if($errors->has('members_number')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('members_number')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if( Session::has('error') ): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong><?php echo e(Session::get('error')); ?></strong>
                        </div>
                    <?php endif; ?>

                    <input value="<?php echo e(count($users)); ?>" type="number" id="alumnos" name="alumnos" hidden>

                    <?php /*
                   $numero_grupos = (count('users')) / $members_number;
                    */?>
                    <div class="grupo box">
                    <div id="FormFields2">
                        <?php if($errors->has('users_id')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('users_id')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>


                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <a   href="createAssignment-Step1" role="button" class="btn btn-primary">Volver al paso 1</a>
                            <button type="submit"   class="btn btn-primary">
                                <?php echo e(__('Siguiente paso')); ?>

                            </button>
                        </div>
                    </div>
                    <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateProfesor', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>