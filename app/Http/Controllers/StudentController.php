<?php

namespace pfg\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use pfg\Models\Assignment;
use Illuminate\Support\Facades\Validator;
use pfg\Models\GroupAssignment;
use pfg\Models\RelUsersGroup;
use pfg\Models\StudentFile;
use Session;
use Illuminate\Support\Facades\Storage;



class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function register(Request $request)
    {

        return view('auth.registerInstructor');
    }

    public function users(Request $request)
    {
        $users = DB::table('users')->paginate(8);
        return view('layouts.usuariosInstructor', compact('users'));
    }

    public function showSubjects()
    {
        $subjects = DB::table('rel_users_subject')
            ->join('subject', 'subject_id', '=', 'subject.id')
            ->join('assignment','subject.id','assignment.subject_id')
            ->join('student_files','assignment.id','=','student_files.assignment_id')
            ->join('group_assignment','student_files.group_id','=','group_assignment.id')
            ->join('rel_users_groups','group_assignment.id','=','rel_users_groups.group_assignment_id')
            ->where('rel_users_subject.users_id',auth()->id())
            ->where('student_files.users_id',auth()->id())
            ->Orwhere('rel_users_subject.users_id',auth()->id())
            ->pluck('subject.name','subject.id');
        return view('layouts.showSubjectsStudent', compact('subjects'));
    }

    public function showResultsStudentSubjects()
    {
        $subjects = DB::table('rel_users_subject')
            ->join('subject', 'subject_id', '=', 'subject.id')
            ->join('assignment','subject.id','assignment.subject_id')
            ->join('student_files','assignment.id','=','student_files.assignment_id')
            ->where('rel_users_subject.users_id',auth()->id())
            ->where('student_files.delivered','=',1)
            ->pluck('subject.name','subject.id');
        return view('layouts.showResultsStudentSubjects', compact('subjects'));
    }

    public function showResultsAssignmentsStudent(Request $request)
    {

        $assignments[] = DB::table('assignment')
            ->select('assignment.*','group_assignment.id as group_assignment_id')
            ->join('student_files','assignment.id','=','student_files.assignment_id')
            ->join('group_assignment','student_files.group_id','=','group_assignment.id')
            ->join('rel_users_groups','group_assignment.id','=','rel_users_groups.group_assignment_id')
            ->Where('rel_users_groups.users_id',auth()->id())
            ->where('student_files.delivered','=',1)
            ->where('student_files.left_attempts','>=',1)
            ->where('assignment.subject_id',$request['subject_id'])
            ->distinct('assignment.id')
            ->get();

        $assignments[] = DB::table('assignment')
            ->select('assignment.*','student_files.group_id as group_assignment_id')
            ->join('student_files','assignment.id','=','student_files.assignment_id')
            ->where('student_files.users_id',auth()->id())
            ->where('student_files.delivered','=',1)
            ->where('student_files.left_attempts','>=',1)
            ->where('assignment.subject_id',$request['subject_id'])
            ->distinct('assignment.id')
            ->get();

       // dd($assignments);
        $subject = DB::table('subject')->where('id',$request['subject_id'])->value('name');
        return view('layouts.showResultsAssignmentsStudent', compact('assignments',$assignments,'subject',$subject));
    }

    public function showAssignmentsStudent(Request $request)
    {

         $assignments[] = DB::table('assignment')
             ->select('assignment.*','group_assignment.id as group_assignment_id')
             ->join('student_files','assignment.id','=','student_files.assignment_id')
             ->join('group_assignment','student_files.group_id','=','group_assignment.id')
             ->join('rel_users_groups','group_assignment.id','=','rel_users_groups.group_assignment_id')
             ->where('student_files.left_attempts','>=',1)
             ->Where('rel_users_groups.users_id',auth()->id())
             ->where('assignment.subject_id',$request['subject_id'])
             ->distinct('assignment.id')
             ->get();

        $assignments[] = DB::table('assignment')
            ->select('assignment.*','student_files.group_id as group_assignment_id')
            ->join('student_files','assignment.id','=','student_files.assignment_id')
            ->where('student_files.users_id',auth()->id())
            ->where('student_files.left_attempts','>=',1)
            ->where('assignment.subject_id',$request['subject_id'])
            ->distinct('assignment.id')
            ->get();
        /* $groups_id = DB::table('group_assignment')
             ->select('group_assignment.id')
             ->join('rel_users_groups','group_assignment.id','=','rel_users_groups.group_assignment_id')
             ->Where('rel_users_groups.users_id',auth()->id())
             ->distinct()
             ->get();
         if(!empty($groups_id)){
             foreach ($groups_id as $group_id) {
                 $assignments[] = DB::table('assignment')
                     ->select('assignment.*')
                     ->join('student_files', 'assignment.id', '=', 'student_files.assignment_id')
                     ->Where('student_files.group_id', $group_id->id)
                     ->where('assignment.subject_id',$request['subject_id'])
                     ->distinct('assignment.id')
                     ->get();
             }
         }*/

         $subject = DB::table('subject')->where('id',$request['subject_id'])->value('name');
         //dd(empty($assignments));
        return view('layouts.showAssignmentsStudent', compact('assignments',$assignments,'subject',$subject));
    }

    public function showStudentsFiles(Request $request)
    {
//dd(empty($request['group_assignment_id']));



        if(count($request->request)>=1){
            $request->session()->put('assignment_id', $request['assignment_id']);
            $request->session()->put('group_assignment_id',$request['group_assignment_id']);

        }

        if(empty($request['group_assignment_id'])){
            $studentsFiles[] = DB::table('assignment')
                ->select('student_files.*')
                ->join('student_files','assignment.id','=','student_files.assignment_id')
                ->where('assignment.id',$request['assignment_id'])
                ->where('student_files.users_id',auth()->id())
                ->get();
        }else{
            $studentsFiles[] = DB::table('assignment')
                ->select('student_files.*')
                ->join('student_files','assignment.id','=','student_files.assignment_id')
                ->Where('student_files.group_id',$request['group_assignment_id'])
                ->get();
        }

        if(count($studentsFiles[0]) < 1){
            if(empty($request->session()->get('group_assignment_id'))){
                $studentsFiles[] = DB::table('assignment')
                    ->select('student_files.*')
                    ->join('student_files','assignment.id','=','student_files.assignment_id')
                    ->where('assignment.id',$request->session()->get('assignment_id'))
                    ->where('student_files.users_id',auth()->id())
                    ->get();
            }else{
                $studentsFiles[] = DB::table('assignment')
                    ->select('student_files.*')
                    ->join('student_files','assignment.id','=','student_files.assignment_id')
                    ->Where('student_files.group_id',$request->session()->get('group_assignment_id'))
                    ->get();
            }
        }




        $assignment = DB::table('assignment')->where('id',$request['assignment_id'])->value('name');
        $subject = DB::table('subject')->join('assignment','subject.id','=','assignment.subject_id')->where('assignment.id',$request['assignment_id'])->value('subject.name');
        //dd($subject);
        return view('layouts.showStudentsFiles', compact('assignment',$assignment,'studentsFiles',$studentsFiles,'subject',$subject));
    }



    public function selectedSubject(Request $request, $subject)
    {
       // dd($subject);
      //  $assignments = DB::table('assignment')->where('created_by',auth()->id())->where('subject_id',$request)->paginate(1);
        //$subject = DB::table('subject')->where('id',$request)->value('name');
     //  dd($assignments);
       // return view('layouts.showAssignments', compact('assignments',$assignments,'subject',$subject));

        $subjects = DB::table('rel_users_subject')->join('subject', 'subject_id', '=', 'subject.id')->where('rel_users_subject.users_id',auth()->id())->pluck('subject.name','subject.id');
        return view('layouts.showSubjectsStudent', compact('subjects'));
    }




    public function sendStudentFiles(Request $request)
    {

        //dd($request);
        foreach ($request['files_name'] as $name) {
            foreach ($request['files_id'] as $item) {
                if ($request['file' . $item] == null) {
                    Session::flash('error', 'Por favor, adjunta todos los archivos para enviar la práctica.');
                    return redirect('/showStudentsFiles');
                }
              /* dd($request['files_name'][1]);
                if (strcmp($request['file' . $item], $name) !== 0) {
                    Session::flash('error', 'Por favor, adjunta todos los archivos con el nombre correcto para enviar la práctica.');
                    return redirect('/showStudentsFiles');
                }*/
            }
        }


            foreach ($request['files_id'] as $item) {
                $studentFile = StudentFile::find($item);
                $fileInstructor = DB::table('assignment')->join('student_files','assignment.id','student_files.assignment_id')->where('student_files.id',$item)->value('assignment.correction_file');
                $language = DB::table('assignment')->join('student_files','assignment.id','student_files.assignment_id')->where('student_files.id',$item)->value('assignment.language');

                $path_completo = '/Applications/MAMP/htdocs/mi-proyecto-laravel/storage/TODO/' . $studentFile->id.'_'.$studentFile->left_attempts;
                $path_save =  $studentFile->id.'_'.$studentFile->left_attempts;
                $studentFile->delivered = 1;
                Storage::put($path_save .'/'. $request['file' . $item]->getClientOriginalName() , file_get_contents($request['file' . $item]));
                //Compilamos el test a realizar
                chdir($path_completo);

                if($language == 'c'){
                    $exec = 'gcc '. $fileInstructor.' -I/lib/include -lcunit  -o ' . $studentFile->fileName;
                    shell_exec($exec);
                    //Ejecutamos el archivo creado de la compilación
                    $ejecutable = './'.$studentFile->fileName;
                    $salida =  $this->PsExecute($ejecutable);
                    if($salida == false){
                        Session::flash('error', 'ERROR: El archivo adjunto puede contener bucles infinitos');
                        return redirect('/showStudentsFiles');
                    }
                    $xml = simplexml_load_file('CUnitAutomated-Results.xml');
                    $studentFile->total = $xml->CUNIT_RUN_SUMMARY->CUNIT_RUN_SUMMARY_RECORD[2]->TOTAL;
                    $studentFile->pass = $xml->CUNIT_RUN_SUMMARY->CUNIT_RUN_SUMMARY_RECORD[2]->SUCCEEDED;
                    $studentFile->fails = $xml->CUNIT_RUN_SUMMARY->CUNIT_RUN_SUMMARY_RECORD[2]->FAILED;
                    $studentFile->score = ($studentFile->pass / $studentFile->total) *10;
                    $path = '/Applications/MAMP/htdocs/mi-proyecto-laravel/storage/TODO/' . $studentFile->id.'_'.$studentFile->left_attempts;
                    mkdir($path);
                    chdir($path);

                }elseif ($language == 'java'){
                    $execCompileStudent = 'javac '. $studentFile->fileName;
                    //dd($execCompileStudent);
                    shell_exec($execCompileStudent);
                    $execCompileInstructor = 'javac -cp /Applications/MAMP/htdocs/mi-proyecto-laravel/public/junit.jar:. '. $fileInstructor;
                    //dd($execCompileInstructor);
                    shell_exec($execCompileInstructor);
                    $fileInstructorRun = basename($fileInstructor, ".java");
                    //$fileStudentRun = basename($studentFile->fileName, ".java");

                    //javac -cp .:/Applications/MAMP/htdocs/mi-proyecto-laravel/public/junit-4.12.jar:/Applications/MAMP/htdocs/mi-proyecto-laravel/public/hamcrest-core-2.1.jar CalculatorTest.java;
                   // dd($fileStudent);
                    $execRun = 'java -cp .:/Applications/MAMP/htdocs/mi-proyecto-laravel/public/junit.jar:/Applications/MAMP/htdocs/mi-proyecto-laravel/public/hamcrest.jar org.junit.runner.JUnitCore '. $fileInstructorRun .' >output.txt';
                    //java -cp .:junit.jar:hamcrest.jar org.junit.runner.JUnitCore
                   //dd($execRun);
                    //$salida =  $this->PsExecute($execRun);
                    shell_exec($execRun);
                    $myfile = fopen("output.txt", "r") or die("Unable to open file!");
                    //$file =  fread($myfile,filesize("output.txt"));
                    $lines = file("output.txt");
                    $KO = 'Failures:';
                    $OK = 'OK';
                    foreach($lines as $line)
                    {
                        if(strpos($line,$KO) !== false){
                           // $line = explode(' ', $line);
                            $line =   preg_split('/[^\d]/', $line, -1, PREG_SPLIT_NO_EMPTY);
                            $studentFile->total = $line[0];
                            $studentFile->fails = $line[1];
                            $studentFile->pass = $studentFile->total - $studentFile->fails;
                            $studentFile->score = ($studentFile->pass / $studentFile->total) *10;
                            //dd($studentFile);
                        }elseif(strpos($line,$OK) !== false){
                            $line =   preg_split('/[^\d]/',  $line, -1, PREG_SPLIT_NO_EMPTY);
                            $studentFile->total = $line[0];
                            $studentFile->fails = '0';
                            $studentFile->pass = $studentFile->total - $studentFile->fails;
                            $studentFile->score = ($studentFile->pass / $studentFile->total) *10;
                            //dd($studentFile);
                        }
                    }

                    //$salida =  $this->PsExecute($execRun);
                   /* if($salida == false){
                        Session::flash('error', 'ERROR: El archivo adjunto puede contener bucles infinitos');
                        return redirect('/showStudentsFiles');
                    }
                    dd($salida);*/
               //  dd('Estamos en prueba');

                }elseif($language == 'c#'){
                    dd('Estamos implementando');
                }
                $studentFile->left_attempts = $studentFile->left_attempts - 1;
                $path = '/Applications/MAMP/htdocs/mi-proyecto-laravel/storage/TODO/' . $studentFile->id.'_'.$studentFile->left_attempts;
                mkdir($path);
                chdir($path);
                $newPath = $studentFile->id.'_'.$studentFile->left_attempts;
                Storage::move($path_save.'/'.$fileInstructor, $newPath.'/'.$fileInstructor);
                $studentFile->save();

            }



        //$request->session()->forget(['assignment_id', 'group_assignment_id']);
        Session::flash('success', 'Práctica entregada correctamente');
        return redirect('/showResultsStudent');
    }

    function PsExecute($command, $timeout = 8, $sleep = 2) {
        // First, execute the process, get the process ID

        $pid = $this->PsExec($command);

        if( $pid === false )
            return false;

        $cur = 0;
        // Second, loop for $timeout seconds checking if process is running
        while( $cur < $timeout ) {
            sleep($sleep);
            $cur += $sleep;
            // If process is no longer running, return true;

         //   echo "\n ---- $cur ------ \n";

            if( !$this->PsExists($pid) )
                return true; // Process must have exited, success!
        }

        // If process is still running after timeout, kill the process and return false
        $this->PsKill($pid);
        return false;
    }

    function PsExec($commandJob) {

        $command = $commandJob.' > /dev/null 2>&1 & echo $!';
        exec($command ,$op);
        $pid = (int)$op[0];

        if($pid!="") return $pid;

        return false;
    }

    function PsExists($pid) {

        exec("ps ax | grep $pid 2>&1", $output);


            foreach($output as $row) {

                $row_array = explode(" ", $row);
            $check_pid = $row_array[0];

            if($pid == $check_pid) {
                return true;
            }

        }

        return false;
    }

    function PsKill($pid) {
        exec("kill -9 $pid", $output);
    }


    public function showResultsStudent(Request $request)
    {
        if(empty($request['assignment_id'])){
            $assignment_id = $request->session()->get('assignment_id');
            $group_assignment_id = $request->session()->get('group_assignment_id');
        }else{
            $assignment_id = $request['assignment_id'];
            $group_assignment_id = $request['group_assignment_id'];
        }
        if(empty($group_assignment_id)){
            $studentsFiles[] = DB::table('assignment')
                ->select('student_files.*')
                ->join('student_files','assignment.id','=','student_files.assignment_id')
                ->where('assignment.id',$assignment_id)
                ->where('student_files.users_id',auth()->id())
                ->get();
        }else{
            $studentsFiles[] = DB::table('assignment')
                ->select('student_files.*')
                ->join('student_files','assignment.id','=','student_files.assignment_id')
                ->Where('student_files.group_id',$group_assignment_id)
                ->get();
        }

        $assignment = DB::table('assignment')->where('id',$assignment_id)->value('name');
        $subject = DB::table('subject')->join('assignment','subject.id','=','assignment.subject_id')->where('assignment.id',$assignment_id)->value('subject.name');
        $request->session()->forget(['assignment_id', 'group_assignment_id']);
        return view('layouts.showResultsStudent', compact('studentsFiles','subject','assignment'));
    }


    /**
     * Show the step 1 Form for creating a new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAssignmentStep1(Request $request)
    {

        $assignment = $request->session()->get('assignment');
        $student = $request->session()->get('student');
       // dd($student);
        $subjects = DB::table('rel_users_subject')->join('subject', 'subject_id', '=', 'subject.id')->where('rel_users_subject.users_id',auth()->id())->pluck('subject.name','subject.id');
       //dd($assignment);
        return view('layouts.createAssignment-Step1',compact('assignment', $assignment,'subjects','student',$student));

    }


    /**
     * Post Request to store step1 info in session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep1(Request $request)
    {


        $validatedData =  $request->validate([
            'name' => 'required',
            'number_files_delivered' => 'required',
            'attempts' => 'required',
            'subject_id' => 'required',
            'language' => 'required',
            'call' => 'required',
        ]);

            $assignment = new Assignment();
            $assignment->fill($validatedData);
            $request->session()->put('assignment', $assignment);
            $total = 0;
            //dd($request);
            for ($i = 1; $i <= $assignment['number_files_delivered']; $i++){
                $student [$i] = new StudentFile();
                $student [$i] ['fileName'] = $request['fileName_'.$i];
                $student [$i] ['weight'] = $request['weight_'.$i];
                $total = $total + $request['weight_'.$i];

                if($student [$i] ['fileName'] == null){
                    Session::flash('error', 'El nombre del fichero '.$i .' está vacío');
                    return redirect('/createAssignment-Step1');
                }
                if($student [$i] ['weight'] == null){
                    Session::flash('error', 'La ponderación del fichero '.$i .' está vacío');
                    return redirect('/createAssignment-Step1');
                }

            }
        if($total != 100){
            Session::flash('error', 'La ponderación del fichero debe sumar 100% en total el valor actual es de '.$total .'%');
            return redirect('/createAssignment-Step1');
        }
            $request->session()->put('student', $student);


        return redirect('/createAssignment-Step2');

    }

    /**
     * Show the step 2 Form for creating a new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAssignmentStep2(Request $request)
    {

        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $groupAssignment = $groupAssignment[1];
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users = DB::table('users')->join('rel_users_subject', 'users.id', '=', 'rel_users_subject.users_id')->where('users.roles_id','3')->where('rel_users_subject.subject_id',$assignment['subject_id'])->get();
        //dd($users);
        return view('layouts.createAssignment-Step2',compact('assignment', $assignment,'groupAssignment',$groupAssignment,'relUsersGroup',$relUsersGroup,'student',$student,'users'));
    }
    /**
     * Post Request to store step1 info in session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep2(Request $request)
    {
        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');



        $validatedData = $request->validate([
                'type' => 'required',
            ]);
        $assignment->fill($validatedData);
        $request->session()->put('assignment', $assignment);
        if(empty($request->session()->get('groupAssignment'))){
        if($request['type'] == 'grupo'){
                $request->validate([
                    'members_number' => 'required',
                ]);
                //TOTAL DE ALUMNOS DE LA ASIGNATURA DE LA PRÁCTICA / TOTAL MIEMBROS POR GRUPO
                $total = ceil($request['alumnos'] /$request['members_number']);

            $k=0;
                for ($i = 1; $i <= $total; $i++){
                    $groupAssignment [$i] = new GroupAssignment();
                    $groupAssignment [$i] ['name'] = 'Grupo: '.$i;
                    $groupAssignment [$i] ['members_number'] = $request['members_number'];
                    for ($j = 1; $j <= $request['members_number']; $j++) {
                        $k++;
                        if($k<=$request['alumnos']){
                            $relUsersGroup[$i] [$j] = new RelUsersGroup();
                            $relUsersGroup[$i] [$j] ['users_id'] = $request['users_id_'.$i.'_'.$j];
                            $infoOK = $request['users_id_'.$i.'_'.$j];
                            if ($infoOK == null) {
                                Session::flash('error', 'El nombre del alumno '. $j . ' del grupo ' . $i . ' está vacío');
                                return redirect('/createAssignment-Step2');
                            }
                        }
                    }
                }
            $request->session()->put('groupAssignment', $groupAssignment);
            $request->session()->put('relUsersGroup', $relUsersGroup);

        }else{
            $users = DB::table('users')->join('rel_users_subject', 'users.id', '=', 'rel_users_subject.users_id')->where('users.roles_id','3')->where('rel_users_subject.subject_id',$assignment['subject_id'])->get();
            $request->session()->put('users', $users);
        }}

        return redirect('/createAssignment-Step3');
    }

    public function createAssignmentStep3(Request $request)
    {
        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users =  $request->session()->get('users');


        return view('layouts.createAssignment-Step3',compact('assignment', $assignment,'groupAssignment',$groupAssignment,'relUsersGroup',$relUsersGroup,'student',$student));

    }
    /**
     * Store product
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep3(Request $request)
    {


        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users =  $request->session()->get('users');



            $request->validate([
            'file' => 'required',
            'delivered_date' => 'required'
        ]);


        $assignment = $request->session()->get('assignment');

        $assignmentSave = new Assignment();
        $assignmentSave->name = $assignment['name'];
        $assignmentSave->delivered_date = $request['delivered_date'];
        $assignmentSave->correction_file = $request['file']->getClientOriginalName();;
		$assignmentSave->number_files_delivered = $assignment['number_files_delivered'];
		$assignmentSave->attempts = $assignment['attempts'];
		$assignmentSave->call = $assignment['call'];
		$assignmentSave->language = $assignment['language'];
		$assignmentSave->type = $assignment['type'];
		$assignmentSave->subject_id = $assignment['subject_id'];
        $assignmentSave->created_by = auth()->id();
        $assignmentSave->save();



        $studentsFile = $request->session()->get('student');
        $relUsersGroup = $request->session()->get('relUsersGroup');
        if($assignmentSave->type == 'grupo') {
            $groupAssignment = $request->session()->get('groupAssignment');
            foreach ($groupAssignment as $group) {
                $groupAssignmentSave = new GroupAssignment();
                $groupAssignmentSave->name = $group['name'];
                $groupAssignmentSave->members_number = $group['members_number'];
                $groupAssignmentSave->assignment_id = $assignmentSave->id;
                $groupAssignmentSave->save();
                $groups_ids [] = $groupAssignmentSave->id;
                foreach ($studentsFile as $student){
                    $studentFileSave = new StudentFile();
                    $studentFileSave->fileName = $student['fileName'];
                    $studentFileSave->weight= $student['weight'];
                    $studentFileSave->assignment_id = $assignmentSave->id;
                    $studentFileSave->left_attempts = $assignmentSave->attempts;
                    $studentFileSave->group_id = $groupAssignmentSave->id;
                    $studentFileSave->save();
                }
            }
            $i=0;
            foreach ($relUsersGroup as $relUserGroup) {
                foreach ($relUserGroup as $rel){
                    $relUsersGroupSave = new RelUsersGroup();
                    $relUsersGroupSave->users_id = $rel['users_id'];
                    $relUsersGroupSave->group_assignment_id = $groups_ids[$i];
                    $relUsersGroupSave->save();
                }
                $i++;
            }
        }else{
            $users =  $request->session()->get('users');
            foreach ($users as $user){
                foreach ($studentsFile as $student){
                    $studentFileSave = new StudentFile();
                    $studentFileSave->fileName = $student['fileName'];
                    $studentFileSave->weight= $student['weight'];
                    $studentFileSave->assignment_id = $assignmentSave->id;
                    $studentFileSave->left_attempts = $assignmentSave->attempts;
                    $studentFileSave->users_id = $user->id;
                    $studentFileSave->save();
                }
            }
        }



        $request->session()->forget(['student', 'assignment','groupAssignment','relUsersGroup']);
        Session::flash('success', 'Práctica creada correctamente');
        return redirect('/createAssignment-Step1');
    }

}
