@extends('layouts.inicio')

@section ('content')

    <form class="login" method="post" action="remember">
        <input type = "hidden" name="_token" value="{{csrf_token()}}">
        <div style="text-align: center">
            <h1>INGRESE NUEVA CONTRASEÑA</h1>
        </div>
        @if ( Session::has('error') )
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
        @endif
        <label for = "password">
            <input required type="password" class="login-input" name="password" maxlength="15" placeholder="Contraseña" autofocus>
        </label>
        <label for = "confirmpassword">
            <input required type="password" class="login-input" name="confirmpassword" maxlength="30" placeholder="Confirmar Contraseña">
        </label>
        <br>
            <input  name="user" value = "<?php echo $idUser; ?>" hidden>
            <input   name="path" value = "<?php echo $path; ?>" hidden>
        <input id="prueba" type="submit" value="Enviar" class="login-button">
    </form>
@stop




