<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>




<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 3</h2>

                <form class="form-horizontal" action="<?php echo e(url('crearPractica-paso3')); ?>" method="post"  enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div>
                        <div class="form-group" style="margin-top: 20px">
                            <label for="delivery_date"><?php echo e(__('Fecha de entrega')); ?></label>
                            <input id="delivery_date" type="datetime-local" min="<?php echo date("Y-m-d\TH:i");?>"  class="form-control<?php echo e($errors->has('delivery_date') ? ' is-invalid' : ''); ?>" name="delivery_date" value="<?php echo e(old('delivery_date')); ?>" requisi>
                            <?php if($errors->has('delivery_date')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('delivery_date')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div style="margin-top: 20px">
                        <label for="file">Archivo de corrección</label>
                        <input id="file" type="file" class="form-control<?php echo e($errors->has('file') ? ' is-invalid' : ''); ?>" name="file" value="<?php echo e(old('file')); ?>" requisi>
                        <?php if($errors->has('file')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('file')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>

                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <a   href="crearPractica-paso1" role="button" class="btn btn-primary">Volver al paso 1</a>
                            <a   href="crearPractica-paso2" role="button" class="btn btn-primary">Volver al paso 2</a>
                            <button type="submit"  style="color: black" class="btn btn-primary">
                                <?php echo e(__('Crear práctica')); ?>

                            </button>
                        </div>
                    </div>
                    <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateProfesor', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>