<?php

namespace pfg\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use pfg\Models\Assignment;
use Illuminate\Support\Facades\Validator;
use pfg\Models\GroupAssignment;
use pfg\Models\RelUsersGroup;
use pfg\Models\StudentFile;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;




class InstructorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function register(Request $request)
    {

        return view('auth.registerInstructor');
    }

    public function users(Request $request)
    {
        $users = DB::table('users')->paginate(8);
        return view('layouts.usuariosInstructor', compact('users'));
    }

    public function showSubjects()
    {
        $subjects = DB::table('rel_users_subject')->join('subject', 'subject_id', '=', 'subject.id')->where('rel_users_subject.users_id',auth()->id())->pluck('subject.name','subject.id');
        return view('layouts.showSubjects', compact('subjects'));
    }

    public function showAssignments(Request $request)
    {
        $assignments = DB::table('assignment')->where('created_by',auth()->id())->where('subject_id',$request['subject_id'])->paginate(10);
       // $assignments = Assignment::where(['created_by' => auth()->id(),])->paginate(1);
        //$assignments = DB::table('assignment')->paginate(1);

      // dd($assignments);

        $subject = DB::table('subject')->where('id',$request['subject_id'])->value('name');
        return view('layouts.showAssignments', compact('assignments',$assignments,'subject',$subject));
    }


    public function selectedSubject($request)
    {
        $assignments = DB::table('assignment')->where('created_by',auth()->id())->where('subject_id',$request)->paginate(1);
        $subject = DB::table('subject')->where('id',$request)->value('name');
     //  dd($assignments);
        return view('layouts.showAssignments', compact('assignments',$assignments,'subject',$subject));
    }



    /**
     * Show the step 1 Form for creating a new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAssignmentStep1(Request $request)
    {

        $assignment = $request->session()->get('assignment');
        $student = $request->session()->get('student');
       // dd($student);
        $subjects = DB::table('rel_users_subject')->join('subject', 'subject_id', '=', 'subject.id')->where('rel_users_subject.users_id',auth()->id())->pluck('subject.name','subject.id');
       //dd($assignment);

        return view('layouts.createAssignment-Step1',compact('assignment', $assignment,'subjects','student',$student));

    }


    /**
     * Post Request to store step1 info in session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep1(Request $request)
    {

        $validatedData =  $request->validate([
            'name' => 'required',
            'number_files_delivered' => 'required',
            'attempts' => 'required',
            'subject_id' => 'required',
            'language' => 'required',
            'call' => 'required',
        ]);

            $assignment = new Assignment();
            $assignment->fill($validatedData);
            $request->session()->put('assignment', $assignment);
            $total = 0;
            //dd($request);
            for ($i = 1; $i <= $assignment['number_files_delivered']; $i++){
                $student [$i] = new StudentFile();
                $student [$i] ['fileName'] = $request['fileName_'.$i];
                $student [$i] ['weight'] = $request['weight_'.$i];
                $total = $total + $request['weight_'.$i];

                if($student [$i] ['fileName'] == null){
                    Session::flash('error', 'El nombre del fichero '.$i .' está vacío');
                    return redirect('/createAssignment-Step1');
                }
                if($student [$i] ['weight'] == null){
                    Session::flash('error', 'La ponderación del fichero '.$i .' está vacío');
                    return redirect('/createAssignment-Step1');
                }

            }
        if($total != 100){
            Session::flash('error', 'La ponderación del fichero debe sumar 100% en total el valor actual es de '.$total .'%');
            return redirect('/createAssignment-Step1');
        }
            $request->session()->put('student', $student);


        return redirect('/createAssignment-Step2');

    }

    /**
     * Show the step 2 Form for creating a new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAssignmentStep2(Request $request)
    {

        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $groupAssignment = $groupAssignment[1];
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users = DB::table('users')->join('rel_users_subject', 'users.id', '=', 'rel_users_subject.users_id')->where('users.roles_id','3')->where('rel_users_subject.subject_id',$assignment['subject_id'])->get();
        //dd($users);
        return view('layouts.createAssignment-Step2',compact('assignment', $assignment,'groupAssignment',$groupAssignment,'relUsersGroup',$relUsersGroup,'student',$student,'users'));
    }
    /**
     * Post Request to store step1 info in session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep2(Request $request)
    {
        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');


        $validatedData = $request->validate([
                'type' => 'required',
            ]);
        $assignment->fill($validatedData);
        $request->session()->put('assignment', $assignment);
        if(empty($request->session()->get('groupAssignment'))){
        if($request['type'] == 'grupo'){
                $request->validate([
                    'members_number' => 'required',
                ]);
                //TOTAL DE ALUMNOS DE LA ASIGNATURA DE LA PRÁCTICA / TOTAL MIEMBROS POR GRUPO
                $total = ceil($request['alumnos'] /$request['members_number']);

            $k=0;
                for ($i = 1; $i <= $total; $i++){
                    $groupAssignment [$i] = new GroupAssignment();
                    $groupAssignment [$i] ['name'] = 'Grupo: '.$i;
                    $groupAssignment [$i] ['members_number'] = $request['members_number'];
                    for ($j = 1; $j <= $request['members_number']; $j++) {
                        $k++;
                        if($k<=$request['alumnos']){
                            $relUsersGroup[$i] [$j] = new RelUsersGroup();
                            $relUsersGroup[$i] [$j] ['users_id'] = $request['users_id_'.$i.'_'.$j];
                            $infoOK = $request['users_id_'.$i.'_'.$j];
                            if ($infoOK == null) {
                                Session::flash('error', 'El nombre del alumno '. $j . ' del grupo ' . $i . ' está vacío');
                                return redirect('/createAssignment-Step2');
                            }
                        }
                    }
                }
            $request->session()->put('groupAssignment', $groupAssignment);
            $request->session()->put('relUsersGroup', $relUsersGroup);

        }else{
            $users = DB::table('users')->join('rel_users_subject', 'users.id', '=', 'rel_users_subject.users_id')->where('users.roles_id','3')->where('rel_users_subject.subject_id',$assignment['subject_id'])->get();
            //dd($users);
            $request->session()->put('users', $users);
        }}

        return redirect('/createAssignment-Step3');
    }

    public function createAssignmentStep3(Request $request)
    {
        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users =  $request->session()->get('users');


        return view('layouts.createAssignment-Step3',compact('assignment', $assignment,'groupAssignment',$groupAssignment,'relUsersGroup',$relUsersGroup,'student',$student));

    }
    /**
     * Store product
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreateAssignmentStep3(Request $request)
    {


        $student = $request->session()->get('student');
        $assignment = $request->session()->get('assignment');
        $groupAssignment= $request->session()->get('groupAssignment');
        $relUsersGroup =  $request->session()->get('relUsersGroup');
        $users =  $request->session()->get('users');



            $request->validate([
            'file' => 'required',
            'delivered_date' => 'required'
        ]);





       /* $file = $request->file('file');
        $file_name = $file->getClientOriginalName();
        $codif ="file --mime-encoding -b ". $file->getClientOriginalName() ."|tr -d '\n'";
        $codif = shell_exec($codif);
        $encode = " iconv -f $codif -t utf8 $file_name > $file_name"."2";
        shell_exec($encode);
        $mover = "mv $file_name"."2". " $file_name";
        shell_exec($mover);
        $eliminar = "rm $file_name"."2";
        shell_exec($eliminar);*/








        //dd($file);
        $assignment = $request->session()->get('assignment');

        $assignmentSave = new Assignment();
        $assignmentSave->name = $assignment['name'];
        $assignmentSave->delivered_date = $request['delivered_date'];
        $assignmentSave->correction_file = $request['file']->getClientOriginalName();;
		$assignmentSave->number_files_delivered = $assignment['number_files_delivered'];
		$assignmentSave->attempts = $assignment['attempts'];
		$assignmentSave->call = $assignment['call'];
		$assignmentSave->language = $assignment['language'];
		$assignmentSave->type = $assignment['type'];
		$assignmentSave->subject_id = $assignment['subject_id'];
        $assignmentSave->created_by = auth()->id();
        $assignmentSave->save();



        $studentsFile = $request->session()->get('student');
        $relUsersGroup = $request->session()->get('relUsersGroup');
        if($assignmentSave->type == 'grupo') {
            $groupAssignment = $request->session()->get('groupAssignment');
            $file = $request->file('file');
            foreach ($groupAssignment as $group) {
                $groupAssignmentSave = new GroupAssignment();
                $groupAssignmentSave->name = $group['name'];
                $groupAssignmentSave->members_number = $group['members_number'];
                $groupAssignmentSave->assignment_id = $assignmentSave->id;
                $groupAssignmentSave->save();
                $groups_ids [] = $groupAssignmentSave->id;
                foreach ($studentsFile as $student){
                    $studentFileSave = new StudentFile();
                    $studentFileSave->fileName = $student['fileName'];
                    $studentFileSave->weight= $student['weight'];
                    $studentFileSave->assignment_id = $assignmentSave->id;
                    $studentFileSave->left_attempts = $assignmentSave->attempts;
                    $studentFileSave->group_id = $groupAssignmentSave->id;
                    $studentFileSave->save();
                    $path = '/Applications/MAMP/htdocs/mi-proyecto-laravel/storage/TODO/' . $studentFileSave->id.'_'.$studentFileSave->left_attempts;;
                    $path2 = $studentFileSave->id.'_'.$studentFileSave->left_attempts;
                    mkdir($path);
                    Storage::put($path2 .'/'. $file->getClientOriginalName() , file_get_contents($file));
                }
            }
            $i=0;
            foreach ($relUsersGroup as $relUserGroup) {
                foreach ($relUserGroup as $rel){
                    $relUsersGroupSave = new RelUsersGroup();
                    $relUsersGroupSave->users_id = $rel['users_id'];
                    $relUsersGroupSave->group_assignment_id = $groups_ids[$i];
                    $relUsersGroupSave->save();
                }
                $i++;
            }
        }else{
            $users =  $request->session()->get('users');
            $file = $request->file('file');
            foreach ($users as $user){
                foreach ($studentsFile as $student){
                    $studentFileSave = new StudentFile();
                    $studentFileSave->fileName = $student['fileName'];
                    $studentFileSave->weight= $student['weight'];
                    $studentFileSave->assignment_id = $assignmentSave->id;
                    $studentFileSave->left_attempts = $assignmentSave->attempts;
                    $studentFileSave->users_id = $user->users_id;
                    $studentFileSave->save();
                    $path = '/Applications/MAMP/htdocs/mi-proyecto-laravel/storage/TODO/' . $studentFileSave->id.'_'.$studentFileSave->left_attempts;;
                    $path2 = $studentFileSave->id.'_'.$studentFileSave->left_attempts;
                    mkdir($path);
                    Storage::put($path2 .'/'. $file->getClientOriginalName() , file_get_contents($file));
                }
            }
        }


        $request->session()->forget(['student', 'assignment','groupAssignment','relUsersGroup']);
        Session::flash('success', 'Práctica creada correctamente');
        return redirect('/createAssignment-Step1');
    }

}
