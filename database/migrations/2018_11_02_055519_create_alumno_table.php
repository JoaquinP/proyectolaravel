<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlumnoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alumno', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('dni')->unique('users_email_unique');
			$table->timestamps();
			$table->integer('roles_id')->unsigned()->index('fk_alumno_roles_idx');
			$table->integer('users_id')->unsigned()->index('fk_alumno_users1_idx');
			$table->primary(['id','users_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alumno');
	}

}
