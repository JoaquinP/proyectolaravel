<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPracticasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('practicas', function(Blueprint $table)
		{
			$table->foreign('asignatura_id', 'fk_practicas_asignatura1')->references('id')->on('asignatura')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('practicas', function(Blueprint $table)
		{
			$table->dropForeign('fk_practicas_asignatura1');
		});
	}

}
