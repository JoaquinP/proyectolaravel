<?php $__env->startSection('title','Formulario'); ?>

<?php $__env->startSection('content'); ?>


    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if(count($practicas)!= 0): ?>
    <form class="form-group" method="POST" action="<?php echo e(dirname($_SERVER['PHP_SELF']) . '/archivos'); ?>" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="card" style="margin-left: auto; margin-right: auto; width: 60rem; margin-top: 70px; padding: 4%;">


                    <div class="row">
                        <div class="col-sm">
                            <h5 class="card-title">Prácticas de la asignatura <?php echo e($practicas[0]->asname); ?></h5>
                            <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                                <select name="practica" size="<?php echo count($practicas)?>">
                                    <?php $__currentLoopData = $practicas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $practica): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($practica->id); ?>"><?php echo e($practica->name); ?> - <?php echo e($practica->convocatoria); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <input name="vista" value="<?php echo e($practicas[0]->vista); ?>" hidden/>
                                <button type="submit" style="margin-top: 2%" class="btn btn-primary">Seleccionar</button>
                            </div>
                        </div>
                    </div>

        </div>
    </form>
           <?php else: ?>
        <div class="row">
            <div class="col-sm">
                <div class="card text-center" style="margin-left: auto; margin-right: auto; width: 50rem; margin-top: 70px; margin-bottom: 30px;">
                <h5 class="card-title">No existen prácticas</h5>
                </div>
            </div>
        </div>
            <?php endif; ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>