<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("select").change(function(){
            $(this).find("option:selected").each(function(){
                var optionValue = $(this).attr("value");
                if(optionValue){
                    $(".box").not("." + optionValue).hide();
                    $("." + optionValue).show();
                } else{
                    $(".box").hide();
                }
            });
        }).change();
    });
</script>

<script type="text/javascript">

    function BuildFormFields2($amount)
    {
        var alumno = document.getElementById("alumnos").value;
        var groups = Math.ceil(alumno/$amount);

        var
            $container = document.getElementById('FormFields2'),
            $item, $field,$campo, $i, $j,$k;
        $container.class = "form-group";

        $container.innerHTML = '';
        var k = 0;
        for ($j = 1; $j < groups+1; $j++) {

            $item = document.createElement('div');
            $item.class = 'form-group';
            $item.style = 'margin-top:20px';

            $campo = document.createElement('h2');
            $campo.innerHTML = 'Grupo ' + $j + ':';
            $item.appendChild($campo);

            for ($i = 1; $i < $amount + 1;  $i++) {
                k++;
                if(k <= alumno){

                    $field = document.createElement('label');
                    $field.innerHTML = 'Introduce el nombre del componente ' + $i + ' del grupo ' + $j ;
                    $item.appendChild($field);

                    $field = document.createElement('input');
                    $field.className = 'form-control';
                    $field.name = 'namepersonGroup[' + $j + '][' + $i + ']';
                    $field.type = 'text';
                    $field.value = '<?php echo e(old('namepersonGroup')); ?>';
                    $item.appendChild($field);
                    $container.appendChild($item);
                }
            }
        }
    }

</script>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 2</h2>

                <form class="form-horizontal" action="<?php echo e(url('crearPractica-paso2')); ?>" method="post"  enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="form-group" style="margin-top: 20px">
                        <label for="type"><?php echo e(__('Tipo de práctica: ')); ?></label>
                        <div>
                            <select name="type" id="type">
                                <option value="individual" selected>Individual</option>
                                <option value="grupo">Grupo</option>
                            </select>
                            <?php if($errors->has('type')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('type')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="grupo box">
                        <div class="form-group" style="margin-top: 20px">
                            <label for="number_group"><?php echo e(__('¿Cuántas personas van a formar el grupo?')); ?></label>
                            <input onkeyup="BuildFormFields2(parseInt(this.value, 10));" id="number_group" type="number" class="form-control<?php echo e($errors->has('number_group') ? ' is-invalid' : ''); ?>" min='1' placeholder=">=1" name="number_group" value="<?php echo e(old('number_group')); ?>" requisi>
                            <?php if($errors->has('number_group')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('number_group')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <input value="15" type="number" id="alumnos" name="alumnos" hidden>

                    <?php /*
                   $numero_grupos = (count('users')) / $number_group;
                    */?>
                    <div class="grupo box">
                    <div id="FormFields2">
                        <?php if($errors->has('namepersonGroup')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('namepersonGroup')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>


                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <a   href="crearPractica-paso1" role="button" class="btn btn-primary">Volver al paso 1</a>
                            <button type="submit"   class="btn btn-primary">
                                <?php echo e(__('Siguiente paso')); ?>

                            </button>
                        </div>
                    </div>
                    <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateProfesor', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>