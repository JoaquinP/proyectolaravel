@extends('layouts.templateProfesor')


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>





<script type="text/javascript">
    function BuildFormFields($amount)
    {

        var
            $container = document.getElementById('FormFields'),
            $item, $field, $i;
            $container.class = "form-group";

            $container.innerHTML = '';

        for ($i = 1; $i < $amount+1; $i++) {
            $item = document.createElement('div');
            $item.class = 'form-group';
            $item.style = 'margin-top:20px';

            $field = document.createElement('label');
            $field.innerHTML = 'Nombre de archivo '+ $i +' a entregar y extensión:';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'fileName.' + $i + '';
            $field.id = 'fileName.' + $i + '';
            $field.type = 'text';
            $field.placeholder ='Ej) practica.c';
            $item.appendChild($field);

            $field = document.createElement('label');
            $field.innerHTML = 'Ponderación del archivo '+ $i +':';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'weight.' + $i + '';
            $field.type = 'number';
            $field.min = '1'
            $field.max = '100'
            $field.placeholder ='100%';
            $item.appendChild($field);
            $container.appendChild($item);
        }
    }

</script>

<script>


    function showHide($amount)
    {
        /*var $number = ' //echo count($student); ?>' ;

        if($number != $amount){
            var
                $container = document.getElementById('FormFields'),
                $item, $field, $i;
            $container.class = "form-group";

            $container.innerHTML = '';

            for ($i = 1; $i < $amount+1; $i++) {
                $item = document.createElement('div');
                $item.class = 'form-group';
                $item.style = 'margin-top:20px';

                $field = document.createElement('label');
                $field.innerHTML = 'Nombre de archivo '+ $i +' a entregar y extensión:';
                $item.appendChild($field);

                $field = document.createElement('input');
                $field.className = 'form-control';
                $field.name = 'fileName.' + $i + '';
                $field.id = 'fileName.' + $i + '';
                $field.type = 'text';
                $field.placeholder ='Ej) practica.c';
                $item.appendChild($field);

                $field = document.createElement('label');
                $field.innerHTML = 'Ponderación del archivo '+ $i +':';
                $item.appendChild($field);

                $field = document.createElement('input');
                $field.className = 'form-control';
                $field.name = 'weight.' + $i + '';
                $field.type = 'number';
                $field.min = '1'
                $field.max = '100'
                $field.placeholder ='100%';
                $item.appendChild($field);
                $container.appendChild($item);
            }
        }else{*/

            x = document.getElementById("fileName_1")
            if (x.style.display === "none") {
                <?php if($student){
                foreach ($student as $stud => $st){?>
                    $("#fileName_{{$stud}}").show();
                    $("#weight_{{$stud}}").show();
                    $("#label1_{{$stud}}").show();
                    $("#label2_{{$stud}}").show();
               <?php }
            } ?>
            } else {
                <?php if($student){
               foreach ($student as $stud => $st){?>
                $("#fileName_{{$stud}}").hide();
                $("#weight_{{$stud}}").hide();
                $("#label1_{{$stud}}").hide();
                $("#label2_{{$stud}}").hide();

                <?php }
                }?>
            }

        //}
    };
</script>

@section('content')
    <?php

    if($assignment['name']){
        $name = $assignment['name'];
    }else{
        $name = old('name');
    }
    if($assignment['number_files_delivered']){
        $number_files_delivered = $assignment['number_files_delivered'];
    }else{
        $number_files_delivered = old('number_files_delivered');
    }
    if($assignment['attempts']){
        $attempts = $assignment['attempts'];
    }else{
        $attempts = old('attempts');
    }
    if($assignment['language']){
        $language = $assignment['language'];
    }else{
        $language = old('language');
    }
    if($assignment['call']){
        $call = $assignment['call'];
    }else{
        $call = old('call');
    }
    ?>


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 1</h2>
                @if ( Session::has('success') )
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>{{ Session::get('success') }}</strong>
                    </div>
                @endif
                <form class="form-horizontal" action="{{ url('createAssignment-Step1') }}" method="post"  enctype="multipart/form-data">
                    @csrf
                    <div  style="margin-top: 20px">
                        <div class="form-group">
                        <label for="name">{{ __('Nombre de la práctica') }}</label>

                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $name }}" requisi autofocus>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    </div>
                    @if (empty($student) || count($student) == 0 || count($student) != $number_files_delivered)
                    <div>
                        <div class="form-group">
                            <label for="number_files_delivered">{{ __('Número de archivos a entregar') }}</label>
                            <input onclick="BuildFormFields(parseInt(this.value, 10));" onkeyup="BuildFormFields(parseInt(this.value, 10));" id="number_files_delivered" type="number" class="form-control{{ $errors->has('number_files_delivered') ? ' is-invalid' : '' }}" min="1" placeholder=">=1" name="number_files_delivered" value="{{ $number_files_delivered }}" requisi autofocus/>

                            @if ($errors->has('number_files_delivered'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('number_files_delivered') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    @if ( Session::has('error') )
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong>{{ Session::get('error') }}</strong>
                        </div>
                    @endif


                    <div id="FormFields">
                @if ($errors->has('fileName'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fileName') }}</strong>
                                    </span>
                            @endif
                    </div>
                    @else


                        @if ($errors->has('number_files_delivered'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('number_files_delivered') }}</strong>
                                    </span>
                        @endif

                        @if ( Session::has('error') )
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>{{ Session::get('error') }}</strong>
                            </div>
                        @endif

                        <div>
                            <div class="form-group">
                                <label for="number_files_delivered">{{ __('Número de archivos a entregar') }}</label>
                                <input onclick="showHide(parseInt(this.value, 10));" id="number_files_delivered" type="number" class="form-control" min="1" placeholder=">=1" name="number_files_delivered" value="{{ $number_files_delivered }}" requisi autofocus/>

                                @if ($errors->has('number_files_delivered'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('number_files_delivered') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <?php $i = 1;?>

                        <div class="hidden">
                        @foreach($student as $st)
                                <div id="hideShow" class="form-group">
                                    <label id="label1_<?php echo $i; ?>" for="fileName_<?php echo $i; ?>">{{ __('Nombre de archivo '.$i .' a entregar y extensión:') }}</label>
                                    <input id="fileName_<?php echo $i; ?>" type="text" class="form-control" min="1" placeholder="Ej) practica.c" name="fileName_<?php echo $i ?>" value="{{ $st->fileName }}" requisi autofocus/>
                                    <label id="label2_<?php echo $i; ?>" for="weight_<?php echo $i; ?>">{{ __('Ponderación del archivo '.$i .':') }}</label>
                                    <input id="weight_<?php echo $i; ?>" type="number" class="form-control" min="1" max="100" placeholder="100%" name="weight_<?php echo $i ?>" value="{{ $st->weight }}" requisi autofocus/>
                                </div>

                                <?php $i++;?>
                        @endforeach
                        </div>

                            <div id="FormFields">
                                @if ($errors->has('fileName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fileName') }}</strong>
                                    </span>
                                @endif
                            </div>
                    @endif

                    <div>
                        <div class="form-group" style="margin-top: 20px">
                        <label for="attempts">{{ __('Intentos') }}</label>
                            <input id="attempts" type="number" class="form-control{{ $errors->has('attempts') ? ' is-invalid' : '' }}" min='1' placeholder=">=1" name="attempts" value="{{ $attempts }}" requisi>
                            @if ($errors->has('attempts'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('attempts') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                            <div class="form-group" style="margin-top: 20px">
                                <label for="language">{{ __('Lenguaje de programación: ') }}</label>
                                <div>
                                <select name="language" id="language">
                                    @if($language == 'c' || $language == '')
                                        <option value="c" selected>C</option>
                                        <option value="c#">C#</option>
                                        <option value="java">Java</option>
                                    @elseif($language == 'c#')
                                        <option value="c">C</option>
                                        <option value="c#" selected>C#</option>
                                        <option value="java">Java</option>
                                    @elseif($language == 'java')
                                        <option value="c">C</option>
                                        <option value="c#">C#</option>
                                        <option value="java" selected>Java</option>
                                    @endif
                                </select>
                                @if ($errors->has('language'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('language') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>

                    <div class="form-group" style="margin-top: 20px">
                        <label for="call">{{ __('Convocatoria: ') }}</label>
                        <div>
                        <select name="call" id="call">
                            @if ($call == 'ordinaria' || $call == '')
                            <option value="ordinaria" selected>Ordinaria</option>
                            <option value="extraordinaria">Extraordinaria</option>
                            @elseif($call == 'extraordinaria')
                                <option value="ordinaria">Ordinaria</option>
                                <option value="extraordinaria" selected>Extraordinaria</option>
                            @endif
                        </select>
                        @if ($errors->has('call'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('call') }}</strong>
                                    </span>
                        @endif
                    </div>
                    </div>
                    <div class="form-group" style="margin-top: 20px">
                        <label for="subject_id">{{ __('Asignatura: ') }}</label>
                        <div>
                        <select name="subject_id" id="subject_id">
                            @foreach($subjects as $subject_id => $subject)
                            <option value="{{$subject_id}}" @if($subject_id ==  $assignment['subject_id']) selected @endif>{{$subject}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('subject_id'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('subject_id') }}</strong>
                                    </span>
                        @endif
                    </div>
                    </div>



                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <button type="submit"  style="margin-left: 70px; width: 200px;" class="btn btn-primary">
                                {{ __('Siguiente paso') }}
                            </button>
                    </div>
                    </div>
                        <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
