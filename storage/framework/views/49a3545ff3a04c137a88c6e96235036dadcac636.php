    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">




<?php $__env->startSection('content'); ?>


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px; margin-left: 150px; margin-right: 150px;">

                <?php if(count($assignments)==0): ?>
                <div class="alert alert-danger" role="alert">
                    <strong>No existen prácticas</strong>
                </div>
                <?php else: ?>
                <h2>Prácticas de la asignatura de <?php echo e($subject); ?></h2>
                <?php endif; ?>


        <?php $i = 0?>

        <?php  foreach ($assignments as $assignment):?>
                    <?php  foreach ($assignment as $as):?>


                    <?php  $i = $i+1;
                //dd($i);?>
        <div  style="overflow: hidden; border: 2px solid #ccc;   text-align: left; background-color: #fafafa;">
            <h2><?php echo $i?>. <?php echo $as->name;?></h2>
            <p style="font-size: 20px"><u><b><?php echo $as->call;?></b></u></p><br><p><b>Fecha de entrega máxima: </b><?php echo $as->delivered_date;?></p>
            <p><b>Número de ficheros a entregar</b> <?php echo $as->number_files_delivered;?></p>
            <p><b>Archivo del profesor:</b> <?php echo $as->correction_file;?></p>
            <p><b>Intentos:</b> <?php echo $as->attempts;?></p>
            <p><b>Lenguaje:</b> <?php echo $as->language;?></p>
            <p><b>Tipo de práctica:</b> <?php echo $as->type;?></p>

                    <form action="<?php echo e(url('showStudentsFiles')); ?>" method="post">
                        <input type = "hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type = "hidden" name="assignment_id" value="<?php echo $as->id?>">
                        <input type = "hidden" name="group_assignment_id" value="<?php echo $as->group_assignment_id?>">
                        <button type="submit" class="btn btn-info">
                            <a class="edit" data-toggle="tooltip">
                                <i style="vertical-align: middle;"class="material-icons">Ver archivos</i>
                            </a>
                        </button>
                    </form>
        </div>

                    <?php endforeach;?>
        <?php endforeach;?>

            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateStudent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>