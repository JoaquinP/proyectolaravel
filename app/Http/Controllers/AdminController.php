<?php

namespace pfg\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use pfg\Models\Practica;
use pfg\Models\User;
use Illuminate\Support\Facades\DB;
use pfg\Http\Requests\StorePracticaRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use PDF;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $users = DB::table('users')->paginate(8);
        return view('layouts.usuarios', compact('users'));

           // $idRol = DB::table('users')->where('id',auth()->id())->value('rol_id');


       // $idRol =  DB::table('users')->where('id',auth()->id())->value('role');
        /*if($idRol == null || $idRol == 1){
          //  $request->user()->authorizeRoles('admin');
            $users = DB::table('users')->paginate(8);
            return view('layouts.usuarios', compact('users'));
        }else
            if($idRol == 2) {
           // $request->user()->authorizeRoles('profesor');
            return redirect('crearPractica-paso1');
        }else if($idRol == 3){
           // $request->user()->authorizeRoles('alumno');
            return redirect('alumno');
        }*/



    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePracticaRequest $request)
    {

        $file = $request->file('file');
        chdir('test');
        $file_name = $file->getClientOriginalName();
        $codif ="file --mime-encoding -b ". $file->getClientOriginalName() ."|tr -d '\n'";
        $codif = shell_exec($codif);
        $encode = " iconv -f $codif -t utf8 $file_name > $file_name"."2";
        shell_exec($encode);
        $mover = "mv $file_name"."2". " $file_name";
        shell_exec($mover);
        $eliminar = "rm $file_name"."2";
        shell_exec($eliminar);


/*
 *
 */
        $idRol = DB::table('profesor')->where('users_id',auth()->id())->value('roles_id');
        if($idRol == null){
            $idRol = DB::table('alumno')->where('users_id',auth()->id())->value('roles_id');
        }
        //dd($idRol);
      //  $idRol =  DB::table('users')->where('id',auth()->id())->value('role');

        if($idRol == 2){
            $practica = new Practica();
            $practica->name = request('name');
            $practica->expired_date = request('expired_date');
            $practica->weight = request('weight');
            $practica->intentos = request('intentos');
            $practica->extension = request('extension');
            $practica->package = request('name') . '-' .  date('Y-m-d');
            $practica->file_execute_profesor = $request->file('file')->getClientOriginalName();
            $practica->idProfesor = auth()->id();
            $practica->idAlumno = '3';
            $practica->file_name = '-';
            $practica->save();
        }else if($idRol == 3){

//          DB::table('role_user')->where('user_id',auth()->id())->update('file_name',$request->file('file')->getClientOriginalName());
            $archivo = Archivo::find(request('id'));
            dd($archivo);
            $archivo->file_name = $request->file('file')->getClientOriginalName();
            $archivo->save();
        }

        //válido para extensiones de c y txt
        if($file->extension() == 'c' || 'txt'){

            /* Indicamos que queremos guardar un nuevo archivo en el disco local.
            En este caso la ruta será: '/Users/joaquin/Documents/PFG/mi-proyecto-laravel/public'
            file_get_contents: Guardar el archivo directamente, sin crear directorios.
            */
            Storage::disk('local')->put($practica->file_execute_profesor,file_get_contents($file));

            //Cambiamos al directorio de test

            //chdir('test');

            //Compilamos el test a realizar
            shell_exec('gcc sum_test.c -I/lib/include -lcunit  -o hola');
            //Ejecutamos el archivo creado de la compilación
            $salida = shell_exec('./hola');

            printf("Tus resultados son:");
            return  dd($salida);
        }else{
            return "extensión no compatible";
        }

    }

   public function cp1251_utf8( $sInput )
    {
        $sOutput = "";

        for ( $i = 0; $i < strlen( $sInput ); $i++ )
        {
            $iAscii = ord( $sInput[$i] );

            if ( $iAscii >= 192 && $iAscii <= 255 )
                $sOutput .=  "&#".( 1040 + ( $iAscii - 192 ) ).";";
            else if ( $iAscii == 168 )
                $sOutput .= "&#".( 1025 ).";";
            else if ( $iAscii == 184 )
                $sOutput .= "&#".( 1105 ).";";
            else
                $sOutput .= $sInput[$i];
        }

        return $sOutput;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $request
     * @return \pfg\User
     */
    protected function createUsers(Request $request)
    {
       // dd($request);

        if ($request['numero'] == 'no') {

            $request->validate( [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'dni' => ['required', 'string', 'max:255', 'unique:users'],
            ]);

        }
        if ($request['numero'] == 'si') {
            $request->validate([
                'file' => 'required|file|max:5000|mimes:xlsx,csv',
            ]);
        }


        $request['password'] = self::random_password();
        $token = self::random_token();
        if ($request['numero'] == 'no') {
            $users = User::create([
                'name' => $request['name'],
                'surname' => $request['surname'],
                'email' => $request['email'],
                'dni' => $request['dni'],
                'roles_id' => $request['role'],
                'password' => Hash::make($request['password']),
                'token'    => $token,
            ]);
            $urlPrimero =  $_SERVER['REQUEST_URI'];
            //$urlPrimero = substr($urlPrimero,0,-13);
            $invitation = "/restablishpass/";
            $path = $urlPrimero . $invitation;
            $url = $path. $token;
            $localhost = 'https://localhost';
            $url = $localhost . $url;
            $email = $request['email'];
            $archivo = fopen("txt/cambioInicial_$email.txt","w");
            $dni = $request['dni'];
            $user= DB::table('users')->where('dni',$dni)->get();
            if( $archivo == false ) {
                echo "Error al crear el archivo";
            }
            else
            {
                // Escribir en el archivo:
                fwrite($archivo, "Su usuario de SSM ha sido creado. El acceso será a través de su DNI\r\n\r\n");
                fwrite($archivo, "Correo: $email\r\n\r\n");
                fwrite($archivo, "Introduzca la contraseña para su usario a través del siguiente link\r\n");
                fwrite($archivo, "URL: $url\r\n");
                fwrite($archivo, "Las condiciones legales serán aceptadas en el momento que realiza el acceso.\r\n\r\n");
                fwrite($archivo, "Condiciones:\r\n\r\n");
                fwrite($archivo, "Lorem ipsum dolor sit amet consectetur adipiscing, elit sociis placerat suspendisse viverra mattis quam, et ultricies curabitur hac facilisis. Risus conubia maecenas platea nec justo tincidunt netus hac elementum odio, parturient nisl nibh cras aliquam sollicitudin mollis dictum curae venenatis curabitur, egestas facilisi ornare rutrum nulla aptent iaculis lectus metus. Et natoque placerat etiam vehicula varius ante tellus aptent bibendum, turpis convallis torquent tincidunt feugiat nam ridiculus scelerisque libero magna, suscipit iaculis sodales faucibus gravida a eleifend mus.
Fames congue nascetur erat montes a purus facilisi taciti, donec maecenas ultrices placerat gravida semper dignissim morbi, eget augue egestas bibendum posuere eleifend urna. Habitasse sociis ad torquent vivamus malesuada auctor class curae congue, tempor himenaeos tellus justo egestas lectus vehicula tincidunt, vel aliquet semper metus quisque libero nam id. Molestie vehicula netus pulvinar dapibus pretium platea justo tincidunt porttitor, donec ac vulputate vitae tortor leo aliquam nascetur sodales ante, per potenti tellus montes quam ad non nunc.\r\n");
                // Fuerza a que se escriban los datos pendientes en el buffer:
                fflush($archivo);
            }
            // Cerrar el archivo:
            fclose($archivo);
            $pdf = PDF::loadView('layouts.userFirstLogIn', compact('email', 'url','user'));
            $nombrePDF = "crearUsuario_".  $email;
            $pdf->download('userFirstLogIn.pdf');
            $pdf->save('pdf/cambioInicial_'.$email.'.pdf');

            //dd($pdf);
            Session::flash('download', 'pdf/cambioInicial_'.$email.'.pdf');

            Session::flash('success', 'Usuario/s cargados correctamente');

            return redirect('users');
            return $users;
            return $pdf->stream($nombrePDF);
           // return view('layouts.usuarios', compact('users'));

           // return $pdf->stream($nombrePDF);
            //dd('aqui sigo');

           // return $users;
        } else {

            if (is_uploaded_file($_FILES['file']['tmp_name'])) {

                $extension = File::extension($request['file']->getClientOriginalName());

                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                    $path = $request['file']->getRealPath();

                    $items = Excel::load($path, function ($reader) {
                    })->get();

                    if (!empty($items) && $items->count()) {
                        $i = 1;
                        foreach ($items as $key => $value) {
                            $email_exist = User::where('email', $value->email)->first();
                            $dni_exist = User::where('dni', $value->dni)->first();
                            $i++;
                            $user = User::where('email','mateos.joaquin@gamaphone.com')->first();
                            if (empty($value->nombre)) {
                                Session::flash('error', 'Usuario/s no cargado/s. El nombre de la columna: ' . $i . ' está vacío.');
                                return $user;
                            }
                            if (empty($value->apellidos)) {
                                Session::flash('error', 'Usuario/s no cargado/s. Los apellidos de la columna: ' . $i . ' están vacíos.');
                                return $user;
                            }
                            if (empty($value->email)) {
                                Session::flash('error', 'Usuario/s no cargado/s. El email de la columna: ' . $i . ' está vacío.');
                                return $user;
                            }
                            if (empty($value->dni)) {
                                Session::flash('error', 'Usuario/s no cargado/s. El dni de la columna: ' . $i . ' está vacío.');
                                return $user;
                            }
                            if (empty($value->rol)) {
                                Session::flash('error', 'Usuario/s no cargado/s. El rol de la columna: ' . $i . ' está vacío.');
                                return $user;
                            }
                            if ($email_exist) {
                                Session::flash('error', 'Usuario/s no cargado/s. El e-mail: ' . $email_exist['email'] . ' ya existe.');
                                return $email_exist;
                            }
                            if ($dni_exist) {
                                Session::flash('error', 'Usuario/s no cargado/s. El dni: ' . $dni_exist['dni'] . ' ya existe.');
                                return $dni_exist;
                            }
                        }
                        foreach ($items as $key => $value) {
                            $value->password = '1234';
                            $users = User::create( [
                                'name' => $value->nombre,
                                'surname' => $value->apellidos,
                                'email' => $value->email,
                                'dni' => $value->dni,
                                'roles_id' => $value->rol,
                                'password' => Hash::make($value->password),
                            ]);



                        }

                        Session::flash('success', 'Usuario/s cargados correctamente');

                    }
                    return $users;

                }
            }

        }
    }

    function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    function random_token( $length = 10 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    public function restablishPass(request $req)
    {
        $data = $req->path();
        $data = substr($data,-10);
        $user = DB::table('users')->where('token', $data)->first();
        if($user){
            $user = get_object_vars($user);
            $idUser = $user['id'];
            $path = $req->path();
            return view('layouts.remember',compact('idUser','path'));
        }

    }
    public function remember(request $req)
    {

        $path = $req->input('path');
        $userId =  intval($req->input('user'));
        $pass =  $req->input('password');
        $confirmpass =  $req->input('confirmpassword');
        if($pass == $confirmpass){
            $token = self::random_token();
          //  $pass = md5($pass);
            DB::table('users')
                ->where('id', $userId)
                ->update(array('token' => $token));
            DB::table('users')
                ->where('id', $userId)
                ->update(array('password' => $pass));
            Session::flash('success', 'Contraseña cambiada correctamente');
            return redirect('/');
        }else{
            Session::flash('error', '¡Las contraseñas no coinciden!');
            return redirect($path);
        }
    }

    public function perdida()
    {
        dd('hola');
        return view ('layouts.perdida');
    }

    public function  sendPerdidaEmail(request $req){
        $email =  $req->input('email');
        $user = DB::table('users')->where('email','like', $email)->first();
        if($user){
            $req->request->add(['token' => $user->token]);
//            Mail::to('mateos.joaquin@gamaphone.com')->send(new \App\Mail\Remember());
//            return view ('home');
            $urlPrimero =  $req->Url();
            $urlPrimero = substr($urlPrimero,0,-16);
            $invitation = "restablishpass/";
            $path = $urlPrimero . $invitation;
//            $path = 'http://127.0.0.1:8000/restablishpass/';
            $token = $req->input('token');
            $url = $path . $token;
            $email = $req->input('email');
            $archivo = fopen("txt/recuperacion_$email.txt","w+b");
            if( $archivo == false ) {
                echo "Error al crear el archivo";
            }
            else
            {
// Escribir en el archivo:
                fwrite($archivo, "Estamos recuperando contraseña de: $email\r\n");
                fwrite($archivo, "URL: $url");
// Fuerza a que se escriban los datos pendientes en el buffer:
                fflush($archivo);
            }
// Cerrar el archivo:
            fclose($archivo);

            $pdf = PDF::loadView('passPDF', compact('email', 'url','user'));
            $nombrePDF = "olvidar_".  $email;
            return $pdf->stream($nombrePDF);
            Session::flash('success', '¡Correo de restauración de contraseña enviado correctamente!');

            return redirect('/');

        }else{
            Session::flash('error', '¡El e-mail no existe!');
            return redirect('perdida');
        }
    }
}
