<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>





<script type="text/javascript">
    function BuildFormFields($amount)
    {

        var
            $container = document.getElementById('FormFields'),
            $item, $field, $i;
            $container.class = "form-group";

            $container.innerHTML = '';

        for ($i = 1; $i < $amount+1; $i++) {
            $item = document.createElement('div');
            $item.class = 'form-group';
            $item.style = 'margin-top:20px';

            $field = document.createElement('label');
            $field.innerHTML = 'Nombre de archivo '+ $i +' a entregar y extensión:';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'fileName.' + $i + '';
            $field.id = 'fileName.' + $i + '';
            $field.type = 'text';
            $field.placeholder ='Ej) practica.c';
            $item.appendChild($field);

            $field = document.createElement('label');
            $field.innerHTML = 'Ponderación del archivo '+ $i +':';
            $item.appendChild($field);

            $field = document.createElement('input');
            $field.className = 'form-control';
            $field.name = 'weight.' + $i + '';
            $field.type = 'number';
            $field.min = '1'
            $field.max = '100'
            $field.placeholder ='100%';
            $item.appendChild($field);
            $container.appendChild($item);
        }
    }

</script>

<script>


    function showHide($amount)
    {
        /*var $number = ' //echo count($student); ?>' ;

        if($number != $amount){
            var
                $container = document.getElementById('FormFields'),
                $item, $field, $i;
            $container.class = "form-group";

            $container.innerHTML = '';

            for ($i = 1; $i < $amount+1; $i++) {
                $item = document.createElement('div');
                $item.class = 'form-group';
                $item.style = 'margin-top:20px';

                $field = document.createElement('label');
                $field.innerHTML = 'Nombre de archivo '+ $i +' a entregar y extensión:';
                $item.appendChild($field);

                $field = document.createElement('input');
                $field.className = 'form-control';
                $field.name = 'fileName.' + $i + '';
                $field.id = 'fileName.' + $i + '';
                $field.type = 'text';
                $field.placeholder ='Ej) practica.c';
                $item.appendChild($field);

                $field = document.createElement('label');
                $field.innerHTML = 'Ponderación del archivo '+ $i +':';
                $item.appendChild($field);

                $field = document.createElement('input');
                $field.className = 'form-control';
                $field.name = 'weight.' + $i + '';
                $field.type = 'number';
                $field.min = '1'
                $field.max = '100'
                $field.placeholder ='100%';
                $item.appendChild($field);
                $container.appendChild($item);
            }
        }else{*/

            x = document.getElementById("fileName_1")
            if (x.style.display === "none") {
                <?php if($student){
                foreach ($student as $stud => $st){?>
                    $("#fileName_<?php echo e($stud); ?>").show();
                    $("#weight_<?php echo e($stud); ?>").show();
                    $("#label1_<?php echo e($stud); ?>").show();
                    $("#label2_<?php echo e($stud); ?>").show();
               <?php }
            } ?>
            } else {
                <?php if($student){
               foreach ($student as $stud => $st){?>
                $("#fileName_<?php echo e($stud); ?>").hide();
                $("#weight_<?php echo e($stud); ?>").hide();
                $("#label1_<?php echo e($stud); ?>").hide();
                $("#label2_<?php echo e($stud); ?>").hide();

                <?php }
                }?>
            }

        //}
    };
</script>

<?php $__env->startSection('content'); ?>
    <?php

    if($assignment['name']){
        $name = $assignment['name'];
    }else{
        $name = old('name');
    }
    if($assignment['number_files_delivered']){
        $number_files_delivered = $assignment['number_files_delivered'];
    }else{
        $number_files_delivered = old('number_files_delivered');
    }
    if($assignment['attempts']){
        $attempts = $assignment['attempts'];
    }else{
        $attempts = old('attempts');
    }
    if($assignment['language']){
        $language = $assignment['language'];
    }else{
        $language = old('language');
    }
    if($assignment['call']){
        $call = $assignment['call'];
    }else{
        $call = old('call');
    }
    ?>


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px">
                <h2>Crear práctica - Paso 1</h2>
                <?php if( Session::has('success') ): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong><?php echo e(Session::get('success')); ?></strong>
                    </div>
                <?php endif; ?>
                <form class="form-horizontal" action="<?php echo e(url('createAssignment-Step1')); ?>" method="post"  enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div  style="margin-top: 20px">
                        <div class="form-group">
                        <label for="name"><?php echo e(__('Nombre de la práctica')); ?></label>

                        <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e($name); ?>" requisi autofocus>
                        <?php if($errors->has('name')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>
                    <?php if(empty($student) || count($student) == 0 || count($student) != $number_files_delivered): ?>
                    <div>
                        <div class="form-group">
                            <label for="number_files_delivered"><?php echo e(__('Número de archivos a entregar')); ?></label>
                            <input onclick="BuildFormFields(parseInt(this.value, 10));" onkeyup="BuildFormFields(parseInt(this.value, 10));" id="number_files_delivered" type="number" class="form-control<?php echo e($errors->has('number_files_delivered') ? ' is-invalid' : ''); ?>" min="1" placeholder=">=1" name="number_files_delivered" value="<?php echo e($number_files_delivered); ?>" requisi autofocus/>

                            <?php if($errors->has('number_files_delivered')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('number_files_delivered')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if( Session::has('error') ): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong><?php echo e(Session::get('error')); ?></strong>
                        </div>
                    <?php endif; ?>


                    <div id="FormFields">
                <?php if($errors->has('fileName')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('fileName')); ?></strong>
                                    </span>
                            <?php endif; ?>
                    </div>
                    <?php else: ?>


                        <?php if($errors->has('number_files_delivered')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('number_files_delivered')); ?></strong>
                                    </span>
                        <?php endif; ?>

                        <?php if( Session::has('error') ): ?>
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong><?php echo e(Session::get('error')); ?></strong>
                            </div>
                        <?php endif; ?>

                        <div>
                            <div class="form-group">
                                <label for="number_files_delivered"><?php echo e(__('Número de archivos a entregar')); ?></label>
                                <input onclick="showHide(parseInt(this.value, 10));" id="number_files_delivered" type="number" class="form-control" min="1" placeholder=">=1" name="number_files_delivered" value="<?php echo e($number_files_delivered); ?>" requisi autofocus/>

                                <?php if($errors->has('number_files_delivered')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('number_files_delivered')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php $i = 1;?>

                        <div class="hidden">
                        <?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $st): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div id="hideShow" class="form-group">
                                    <label id="label1_<?php echo $i; ?>" for="fileName_<?php echo $i; ?>"><?php echo e(__('Nombre de archivo '.$i .' a entregar y extensión:')); ?></label>
                                    <input id="fileName_<?php echo $i; ?>" type="text" class="form-control" min="1" placeholder="Ej) practica.c" name="fileName_<?php echo $i ?>" value="<?php echo e($st->fileName); ?>" requisi autofocus/>
                                    <label id="label2_<?php echo $i; ?>" for="weight_<?php echo $i; ?>"><?php echo e(__('Ponderación del archivo '.$i .':')); ?></label>
                                    <input id="weight_<?php echo $i; ?>" type="number" class="form-control" min="1" max="100" placeholder="100%" name="weight_<?php echo $i ?>" value="<?php echo e($st->weight); ?>" requisi autofocus/>
                                </div>

                                <?php $i++;?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                            <div id="FormFields">
                                <?php if($errors->has('fileName')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('fileName')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                    <?php endif; ?>

                    <div>
                        <div class="form-group" style="margin-top: 20px">
                        <label for="attempts"><?php echo e(__('Intentos')); ?></label>
                            <input id="attempts" type="number" class="form-control<?php echo e($errors->has('attempts') ? ' is-invalid' : ''); ?>" min='1' placeholder=">=1" name="attempts" value="<?php echo e($attempts); ?>" requisi>
                            <?php if($errors->has('attempts')): ?>
                                <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('attempts')); ?></strong>
                                    </span>
                            <?php endif; ?>
                        </div>
                    </div>


                            <div class="form-group" style="margin-top: 20px">
                                <label for="language"><?php echo e(__('Lenguaje de programación: ')); ?></label>
                                <div>
                                <select name="language" id="language">
                                    <?php if($language == 'c' || $language == ''): ?>
                                        <option value="c" selected>C</option>
                                        <option value="c#">C#</option>
                                        <option value="java">Java</option>
                                    <?php elseif($language == 'c#'): ?>
                                        <option value="c">C</option>
                                        <option value="c#" selected>C#</option>
                                        <option value="java">Java</option>
                                    <?php elseif($language == 'java'): ?>
                                        <option value="c">C</option>
                                        <option value="c#">C#</option>
                                        <option value="java" selected>Java</option>
                                    <?php endif; ?>
                                </select>
                                <?php if($errors->has('language')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('language')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            </div>

                    <div class="form-group" style="margin-top: 20px">
                        <label for="call"><?php echo e(__('Convocatoria: ')); ?></label>
                        <div>
                        <select name="call" id="call">
                            <?php if($call == 'ordinaria' || $call == ''): ?>
                            <option value="ordinaria" selected>Ordinaria</option>
                            <option value="extraordinaria">Extraordinaria</option>
                            <?php elseif($call == 'extraordinaria'): ?>
                                <option value="ordinaria">Ordinaria</option>
                                <option value="extraordinaria" selected>Extraordinaria</option>
                            <?php endif; ?>
                        </select>
                        <?php if($errors->has('call')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('call')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>
                    <div class="form-group" style="margin-top: 20px">
                        <label for="subject_id"><?php echo e(__('Asignatura: ')); ?></label>
                        <div>
                        <select name="subject_id" id="subject_id">
                            <?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subject_id => $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($subject_id); ?>" <?php if($subject_id ==  $assignment['subject_id']): ?> selected <?php endif; ?>><?php echo e($subject); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('subject_id')): ?>
                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('subject_id')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                    </div>



                    <div style="margin-top: 20px">
                        <div class="form-group ">
                            <button type="submit"  style="margin-left: 70px; width: 200px;" class="btn btn-primary">
                                <?php echo e(__('Siguiente paso')); ?>

                            </button>
                    </div>
                    </div>
                        <div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateProfesor', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>