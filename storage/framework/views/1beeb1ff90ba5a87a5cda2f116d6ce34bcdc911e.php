    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">




<?php $__env->startSection('content'); ?>


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-4 col-sm-offset-4" style="margin-top: 140px; margin-left: 150px; margin-right: 150px;">

                <?php if(count($studentsFiles)==0): ?>
                <div class="alert alert-danger" role="alert">
                    <strong>No existen prácticas</strong>
                </div>
                <?php else: ?>
                <h2>Práctica <?php echo e($assignment); ?> de la asignatura de: <?php echo e($subject); ?></h2>
                <?php endif; ?>
                    <?php if( Session::has('error') ): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong><?php echo e(Session::get('error')); ?></strong>
                        </div>
                    <?php endif; ?>

        <?php $i = 0?>
                    <form class="form-horizontal" action="<?php echo e(url('sendStudentFiles')); ?>" method="post"  enctype="multipart/form-data">
                        <input type = "hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <?php  foreach ($studentsFiles as $studentsFile):?>
                    <?php  foreach ($studentsFile as $studentFile):?>


                    <?php  $i = $i+1;
                //dd($i);?>
        <div  style="overflow: hidden; border: 2px solid #ccc;   text-align: left; background-color: #fafafa;">
            <h2><?php echo $i?>. <?php echo $studentFile->fileName;?></h2>
            <p style="font-size: 20px"><u><b><?php echo $studentFile->fileName;?></b></u></p><br><p><b>Ponderación del archivo: </b><?php echo $studentFile->weight;?></p>
            <p><b>Intentos restantes:</b> <?php echo $studentFile->left_attempts;?></p>
            <div style="margin-top: 20px">
                <div class="form-group" style="margin-top: 20px">
                <label for="file<?php echo e($studentFile->id); ?>">Archivo del alumno:</label>
                <input id="file<?php echo e($studentFile->id); ?>" style="height: 43px; line-height: 25px" type="file" class="form-control<?php echo e($errors->has('file') ? ' is-invalid' : ''); ?>" name="file<?php echo e($studentFile->id); ?>" value="<?php echo e(old('file')); ?>" requisi>
                <?php if($errors->has('file')): ?>
                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('file')); ?></strong>
                                    </span>
                <?php endif; ?>
            </div>
            </div>
        </div>

                        <input type = "hidden" name="files_id[]" value="<?php echo e($studentFile->id); ?>">
                        <input type = "hidden" name="files_name[]" value="<?php echo e($studentFile->fileName); ?>">
                    <?php endforeach;?>
        <?php endforeach;?>
                    <input type = "hidden" name="group_assignment_id" value="<?php echo $studentFile->group_id?>">
                    <button style="margin-top: 20px" type="submit"  style="color: black" class="btn btn-primary">
                        <?php echo e(__('Enviar práctica')); ?>

                    </button>
                    </form>

            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.templateStudent', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>